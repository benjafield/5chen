# 5chen Image Board

Thank mr bok.

## Installation

I don't advise using this Laravel build on a local installation of Apache, PHP and DB. I would instead set up a working version of Laravel Homestead, then clone this repo into that.

It is now at a point where it can be cloned and worked on.

Once cloned, open up homestead with terminal, and run the following command from within this application's root directory:


```
#!CLI

php artisan key:generate
```

Then set up the .env file with correct database credentials, and run:


```
#!CLI

php artisan migrate
```

Happy coding :P