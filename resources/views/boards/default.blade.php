@extends('layouts.app')
@section('title', $board->name)

@section('content')

@foreach($board->threads()->get() as $thread)
<div class="Thread" id="thread-{{ $thread->id }}">
	<div class="Thread__file">
		<a href="http://cdn.thenewfragranceformen.com/assets/ads/temp/N8epPIsGOX_rACGB.jpg">
			<img src="http://cdn.thenewfragranceformen.com/assets/ads/temp/N8epPIsGOX_rACGB.jpg">
		</a>
	</div>
	<div class="Thread__info">
		<strong>Anonymous</strong> - {{ $thread->created_at->format('d/m/Y') }} @ {{ $thread->created_at->format('H:i') }}
	</div>
	<div class="Thread__content">
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, sit.
	</div>
</div>
@endforeach

@stop