<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $fillable = ['user_id', 'name', 'path', 'description'];

    /**
     * A board has many threads.
     */
    public function threads()
    {
    	return $this->hasMany('App\Thread');
    }

    /**
     * A board belongs to a user.
     */
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
