<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['user_id', 'thread_id', 'anonymous', 'content'];

    /**
     * A post belongs to a thread.
     */
    public function thread()
    {
    	return $this->belongsTo('App\Board');
    }

    /**
     * A post belongs to a user.
     */
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
