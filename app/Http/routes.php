<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'AppController@index');
Route::get('/login', 'Auth\AuthController@getLogin');
Route::get('/register', 'Auth\AuthController@getResister');
Route::get('/logout', 'Auth\AuthController@logout');

/**
 * Auth POST Requests
 */
Route::post('/login', 'Auth\AuthController@postLogin');
Route::post('/register', 'Auth\AuthController@postRegister');