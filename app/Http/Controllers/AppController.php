<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Board;

class AppController extends Controller
{
    
	/**
	 * Display the homepage/1st page of the board.
	 *
	 * @return \Illuminate\Http\Response
	 *
	 */
	public function index()
	{
		$board = Board::find(1);

		return view('boards.default', compact('board'));
	}

}
